<?php 
namespace Package\Nothing628\Sipus\Syncer;

use Package\Nothing628\Sipus\Models\Anggota;
use Users;

class Syncer {
	public function __construct()
	{
		//
	}

	public function sync()
	{
		//
	}

	public function add($user)
	{
		$anggota = new Anggota;
		$anggota->user_id = $user->id;
		$anggota->name = $user->name;
		$anggota->email = $user->email;
		$anggota->avatar = $user->avatar;
		$anggota->save();
	}

	public function delete($user)
	{
		$anggota = Anggota::where('user_id', $user->id)->get();

		if ($anggota->count() > 0) {
			$anggota = $anggota->first();
			$anggota->delete();

			return true;
		}

		return false;
	}
}