<?php 
namespace Package\Nothing628\Sipus\Models;

use Illuminate\Database\Eloquent\Model;

class Format extends Model {
	public $incrementing = false;
	public $timestamps = false;
	public $fillable = ['id', 'format', 'keterangan'];
}