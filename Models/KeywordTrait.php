<?php 
namespace Package\Nothing628\Sipus\Models;

trait KeywordTrait {
	public function setKeyword($keyword, $delimiter = ',')
	{
		$keywords = explode($delimiter, $keyword);

		return $this->setKeywordArray($keywords);
	}

	public function setKeywordArray($keywords = [])
	{
		if (! $this->exists) {
			throw new Exception("Model not been saved.", 1);
		}

		$trimmed = [];
		$exists = $this->getKeywordArray();

		foreach ($keywords as $keyword)
		{
			$trimmed[] = trim($keyword);
		}

		$toAdd = array_diff($trimmed, $exists);

		foreach ($toAdd as $add) {
			$model = Keyword::where('keyword', $add)->get();

			if ($model->count() > 0) {
				$model = $model->first();
			} else {
				$model = new Keyword;
				$model->keyword = $add;
			}

			$this->keywords()->save($model);
		}
		
		$this->load('keywords');

		return $this;
	}

	public function getKeyword($delimiter = ',')
	{
		return implode($delimiter, $this->getKeywordArray());
	}

	public function getKeywordArray()
	{
		if (! $this->exists) return [];

		$keywords = $this->keywords;
		$result = [];

		foreach ($keywords as $key) {
			$result[] = $key->keyword;
		}

		return $result;
	}

	public function keywords()
	{
		return $this->morphToMany(Keyword::class, 'keywordable');
	}
}