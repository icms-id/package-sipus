<?php 
namespace Package\Nothing628\Sipus\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model {

	public $timestamps = false;

	public function pustaka()
    {
        return $this->morphedByMany(Pustaka::class, 'keywordable');
    }

    public function keywordable()
    {
        return $this->morphTo();
    }
}