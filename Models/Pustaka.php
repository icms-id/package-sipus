<?php 
namespace Package\Nothing628\Sipus\Models;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Pustaka extends Model {
	use KeywordTrait;

	protected $with = ['penulis', 'penerbit', 'format', 'catalog'];

	public function listpustaka()
	{
		return $this->hasMany(PustakaList::class);
	}

	public function penulis()
	{
		return $this->belongsTo(Penulis::class);
	}

	public function penerbit()
	{
		return $this->belongsTo(Penerbit::class);
	}

	public function format()
	{
		return $this->belongsTo(Format::class);
	}

	public function catalog()
	{
		return $this->belongsTo(Catalog::class);
	}

	public function addPustaka($jml)
	{
		if ($this->exists) {
			for ($i = 1; $i<=$jml; $i++)
			{
				$newpustaka = new PustakaList;
				$newpustaka->uuid = Uuid::generate(4);

				$this->listpustaka()->save($newpustaka);
			}
		}
	}

	protected static function boot() {
		parent::boot();

		static::deleting(function($pustaka) {
			 $pustaka->listpustaka()->delete();
		});
	}
}