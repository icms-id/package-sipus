<?php 
namespace Package\Nothing628\Sipus\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model {
	public $incrementing = false;
	public $timestamps = false;
	public $fillable = ['id', 'katalog', 'keterangan'];
}