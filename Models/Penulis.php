<?php 
namespace Package\Nothing628\Sipus\Models;

use Illuminate\Database\Eloquent\Model;

class Penulis extends Model {
	use MetableTrait;

	public $metafield = 'meta';
	public $metadata = ['telepon', 'email'];
	public $fillable = ['nama', 'alamat', 'keterangan', 'telepon', 'email'];
	public $timestamps = false;
	protected $casts = ['meta' => 'array'];

	public function pustaka()
	{
		return $this->hasMany(Pustaka::class);
	}
}