<?php 

return [
	'<i class="fa fa-book"></i> Perpustakaan' => [
		'Data' => [
			'Anggota'					=> 'sipus.anggota',
			'Pustaka'					=> 'sipus.pustaka',
			'Penulis'					=> 'sipus.penulis',
			'Penerbit'					=> 'sipus.penerbit',
			'Katalog'					=> 'sipus.katalog',
			'Format'					=> 'sipus.format',
		],
		'Transaksi' => [
			'Peminjaman'				=> 'sipus.peminjaman',
			'Pengembalian'				=> 'sipus.pengembalian',
		],
		'Laporan' => [
			'Peminjaman'				=> 'sipus.laporan.peminjaman',
			'Pengembalian'				=> 'sipus.laporan.pengembalian',
			'Denda'						=> 'sipus.laporan.denda',
		],
	]
];