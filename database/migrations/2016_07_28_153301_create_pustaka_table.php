<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePustakaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pustakas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penulis_id')->unsigned()->nullable();
            $table->integer('penerbit_id')->unsigned()->nullable();
            $table->string('format_id', 5);
            $table->string('catalog_id', 5);
            $table->string('judul');
            $table->string('deskripsi')->nullable();
            $table->string('thn_terbit', 4)->nullable();
            $table->string('cover')->nullable();
            $table->decimal('hrg')->unsigned();
            $table->timestamps();
        });

        Schema::create('pustaka_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pustaka_id');
            $table->string('uuid');
            $table->tinyInteger('status')->default(1);      //1 => Tersedia, 0 => Dipinjam, -1 => Rusak/Hilang
            $table->timestamps();

            $table->unique('uuid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pustaka_lists');
        Schema::dropIfExists('pustakas');
    }
}
