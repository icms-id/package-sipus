<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->string('keyword');
        });
        Schema::create('keywordables', function (Blueprint $table) {
            $table->integer('keyword_id')->unsigned();
            $table->integer('keywordable_id')->unsigned();
            $table->string('keywordable_type');
        });
        Schema::create('catalogs', function (Blueprint $table) {
            $table->string('id', 5);
            $table->string('katalog', 60);
            $table->string('keterangan');

            $table->primary(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogs');
        Schema::dropIfExists('keywordables');
        Schema::dropIfExists('keywords');
    }
}
