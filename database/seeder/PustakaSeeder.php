<?php 

use Illuminate\Database\Seeder;

class PustakaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('formats')->insert(['id' => '00001', 'format' => 'BU', 'keterangan' => 'Buku']);
    	DB::table('formats')->insert(['id' => '00002', 'format' => 'LU', 'keterangan' => 'Lembar']);

        DB::table('catalogs')->insert(['id' => '0000A', 'katalog' => 'Sejarah dan Budaya', 'keterangan' => 'Sejarah dan Budaya']);
        DB::table('catalogs')->insert(['id' => '0000B', 'katalog' => 'Bahasa dan Sastra', 'keterangan' => 'Dian sastro']);

        DB::table('penerbits')->insert(['nama' => 'Eir Aoi', 'alamat' => 'Home Test', 'keterangan' => 'Home test', 'meta' => 
            '{"email": "digitalentertaiment@gmail.com", "telepon": "081923135113"}']);

        DB::table('penulis')->insert(['nama' => 'Ukan Jik', 'alamat' => 'test', 'keterangan' => 'test', 'meta' =>
            '{"email": "yogyphang@gmail.com", "telepon": "082993299222"}']);

        DB::table('pustakas')->insert([
            'penulis_id' => 1,
            'penerbit_id' => 1,
            'format_id' => '00001',
            'catalog_id' => '0000A',
            'judul' => 'Sampel data',
            'deskripsi' => 'Sample data',
            'thn_terbit' => '2011',
            'cover' => 'e1280614-7ed3-4997-95ae-3a0015f73af1.jpg',
            'hrg' => 200000]);
    }
}