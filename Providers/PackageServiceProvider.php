<?php 

namespace Package\Nothing628\Sipus\Providers;

use ICMS\Package\PackageServiceProvider as ServiceProvider;

class PackageServiceProvider extends ServiceProvider {
	protected $providesRoute = [
		'List Buku' => 'sipus.book.ok'
	];

	public function boot()
	{
		//
	}

	public function register()
	{
		parent::register();
	}
}