<?php 

namespace Package\Nothing628\Sipus\Providers;

use ICMS\Package\RouteServiceProvider as ServiceProvider;
use Package\Nothing628\Sipus\Http\Middleware\AdminMiddleware;

class RouteServiceProvider extends ServiceProvider {
	protected $namespace = 'Package\Nothing628\Sipus\Http\Controllers';
	protected $slug = 'sipus';
	protected $middleware = [
		'sipus' => AdminMiddleware::class
	];
}