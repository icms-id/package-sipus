<?php 

namespace Package\Nothing628\Sipus\Providers;

use ICMS\Package\EventServiceProvider as ServiceProvider;
use Package\Nothing628\Sipus\Listeners\UserCreated;
use Package\Nothing628\Sipus\Listeners\UserDeleted;

class EventServiceProvider extends ServiceProvider {
	protected $listen = [
		'user.role.changed' => [
			UserCreated::class
		],
		'user.deleted' => [
			UserDeleted::class
		],
	];
}