<?php 

Route::get('/', ['as' => 'sipus.admin', 'uses' => 'AdminController@index']);

Route::group(['prefix' => 'pustaka'], function () {
	Route::get('/', ['as' => 'sipus.pustaka', 'uses' => 'BookController@index']);
	Route::get('add', ['as' => 'sipus.pustaka.add', 'uses' => 'BookController@add']);
	Route::post('save', ['as' => 'sipus.pustaka.save', 'uses' => 'BookController@save']);
	Route::post('update', ['as' => 'sipus.pustaka.update', 'uses' => 'BookController@update']);
	Route::post('ajax', ['as' => 'sipus.pustaka.ajax', 'uses' => 'BookController@ajax']);
	Route::get('delete/{id?}', ['as' => 'sipus.pustaka.delete', 'uses' => 'BookController@delete']);
	Route::get('edit/{id?}', ['as' => 'sipus.pustaka.edit', 'uses' => 'BookController@edit']);
	Route::get('detail/{id?}', ['as' => 'sipus.pustaka.detail', 'uses' => 'BookController@detail']);

	Route::post('list/ajax/{id?}', ['as' => 'sipus.pustaka.list.ajax', 'uses' => 'BookController@ajaxlist']);
	Route::get('list/delete/{id?}', ['as' => 'sipus.pustaka.list.delete', 'uses' => 'BookController@deleteList']);
	Route::get('list/barcode/{id?}', ['as' => 'sipus.pustaka.list.barcode', 'uses' => 'BookController@generateBarcode']);
	Route::post('list/add/{id?}', ['as' => 'sipus.pustaka.list.add', 'uses' => 'BookController@addListPustaka']);
});

Route::group(['prefix' => 'penulis'], function () {
	Route::get('/', ['as' => 'sipus.penulis', 'uses' => 'PenulisController@index']);
	Route::get('add', ['as' => 'sipus.penulis.add', 'uses' => 'PenulisController@add']);
	Route::post('save', ['as' => 'sipus.penulis.save', 'uses' => 'PenulisController@save']);
	Route::post('update', ['as' => 'sipus.penulis.update', 'uses' => 'PenulisController@update']);
	Route::post('ajax', ['as' => 'sipus.penulis.ajax', 'uses' => 'PenulisController@ajax']);
	Route::get('delete/{id?}', ['as' => 'sipus.penulis.delete', 'uses' => 'PenulisController@delete']);
	Route::get('edit/{id?}', ['as' => 'sipus.penulis.edit', 'uses' => 'PenulisController@edit']);
});

Route::group(['prefix' => 'penerbit'], function () {
	Route::get('/', ['as' => 'sipus.penerbit', 'uses' => 'PenerbitController@index']);
	Route::get('add', ['as' => 'sipus.penerbit.add', 'uses' => 'PenerbitController@add']);
	Route::post('save', ['as' => 'sipus.penerbit.save', 'uses' => 'PenerbitController@save']);
	Route::post('update', ['as' => 'sipus.penerbit.update', 'uses' => 'PenerbitController@update']);
	Route::post('ajax', ['as' => 'sipus.penerbit.ajax', 'uses' => 'PenerbitController@ajax']);
	Route::get('delete/{id?}', ['as' => 'sipus.penerbit.delete', 'uses' => 'PenerbitController@delete']);
	Route::get('edit/{id?}', ['as' => 'sipus.penerbit.edit', 'uses' => 'PenerbitController@edit']);
});

Route::group(['prefix' => 'katalog', 'middleware' => 'sipus'], function () {
	Route::get('/', ['as' => 'sipus.katalog', 'uses' => 'KatalogController@index']);
	Route::get('add', ['as' => 'sipus.katalog.add', 'uses' => 'KatalogController@add']);
	Route::post('save', ['as' => 'sipus.katalog.save', 'uses' => 'KatalogController@save']);
	Route::post('update', ['as' => 'sipus.katalog.update', 'uses' => 'KatalogController@update']);
	Route::post('ajax', ['as' => 'sipus.katalog.ajax', 'uses' => 'KatalogController@ajax']);
	Route::get('delete/{id?}', ['as' => 'sipus.katalog.delete', 'uses' => 'KatalogController@delete']);
	Route::get('edit/{id?}', ['as' => 'sipus.katalog.edit', 'uses' => 'KatalogController@edit']);
});

Route::group(['prefix' => 'format'], function () {
	Route::get('/', ['as' => 'sipus.format', 'uses' => 'FormatController@index']);
	Route::get('add', ['as' => 'sipus.format.add', 'uses' => 'FormatController@add']);
	Route::post('save', ['as' => 'sipus.format.save', 'uses' => 'FormatController@save']);
	Route::post('update', ['as' => 'sipus.format.update', 'uses' => 'FormatController@update']);
	Route::post('ajax', ['as' => 'sipus.format.ajax', 'uses' => 'FormatController@ajax']);
	Route::get('delete/{id?}', ['as' => 'sipus.format.delete', 'uses' => 'FormatController@delete']);
	Route::get('edit/{id?}', ['as' => 'sipus.format.edit', 'uses' => 'FormatController@edit']);
});

Route::group(['prefix' => 'anggota'], function () {
	Route::get('/', ['as' => 'sipus.anggota', 'uses' => 'AnggotaController@index']);
	Route::post('ajax', ['as' => 'sipus.anggota.ajax', 'uses' => 'AnggotaController@ajax']);
	Route::get('detail/{id?}', ['as' => 'sipus.anggota.detail', 'uses' => 'AnggotaController@detail']);
	Route::get('edit/{id?}', ['as' => 'sipus.anggota.edit', 'uses' => 'AnggotaController@edit']);
	Route::get('delete/{id?}', ['as' => 'sipus.anggota.delete', 'uses' => 'AnggotaController@delete']);
});

Route::get('peminjaman', ['as' => 'sipus.peminjaman', 'uses' => 'TransaksiController@peminjaman']);
Route::get('pengembalian', ['as' => 'sipus.pengembalian', 'uses' => 'TransaksiController@pengembalian']);

Route::group(['prefix' => 'laporan'], function () {
	Route::get('peminjaman', ['as' => 'sipus.laporan.peminjaman', 'uses' => 'ReportController@peminjaman']);
	Route::get('pengembalian', ['as' => 'sipus.laporan.pengembalian', 'uses' => 'ReportController@pengembalian']);
	Route::get('denda', ['as' => 'sipus.laporan.denda', 'uses' => 'ReportController@denda']);
});