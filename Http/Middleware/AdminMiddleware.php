<?php 
namespace Package\Nothing628\Sipus\Http\Middleware;

use Closure;

class AdminMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		// if (Auth::guard($guard)->check()) {
		// 	return redirect('/');
		// }

		return $next($request);
	}
}