<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package\Nothing628\Sipus\Models\Anggota;
use Package;
use Setting;

class AnggotaController extends Controller {

	public function index()
	{
		return Package::view('anggota.list', [], 'Daftar Anggota');
	}

	public function refreshAnggota()
	{
		//
	}

	public function ajax(Request $request)
	{
		$result = [];
		$pustakas = Anggota::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $pustakas->count();
		$result['recordsFiltered'] = $pustakas->count();
		$result['data'] = [];

		$pustakas = $pustakas->splice($request->start ,$request->length);

		foreach ($pustakas as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Name' => $value->name,
				'Email' => $value->email,
			];
		}

		return response()->json($result);
	}

	public function edit($id = null)
	{
		//
	}

	public function delete($id = null)
	{
		//
	}

	public function detail($id = null)
	{
		//
	}
}