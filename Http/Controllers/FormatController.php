<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package\Nothing628\Sipus\Models\Format;
use Package;

class FormatController extends Controller {

	public function index()
	{
		return Package::view('format.list', [], 'Format Pustaka');
	}

	public function add()
	{
		return Package::view('format.add', [], 'Tambah Format');
	}

	public function save(Request $request)
	{
		$format = new Format;
		$format->fill($request->all());
		$format->save();

		return redirect()->to(Package::route('sipus.format'));
	}

	public function ajax(Request $request)
	{
		$result = [];
		$formats = Format::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $formats->count();
		$result['recordsFiltered'] = $formats->count();
		$result['data'] = [];

		$formats = $formats->splice($request->start ,$request->length);

		foreach ($formats as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Format' => $value->format,
				'Keterangan' => $value->keterangan,
			];
		}

		return response()->json($result);
	}

	public function delete($id = null)
	{
		$format = Format::find($id);

		if ($format) {
			$format->delete();
		}

		return redirect()->to(Package::route('sipus.format'));
	}
	
	public function edit($id = null)
	{
		$format = Format::find($id);

		if ($format) {
			return Package::view('format.edit', ['format' => $format], 'Edit Format');
		}

		return redirect()->to(Package::route('sipus.format'));
	}

	public function update(Request $request)
	{
		$format = Format::find($request->input('id'));

		if ($format) {
			$format->fill($request->all());
			$format->save();

			return redirect()->to(Package::route('sipus.format'));
		}

		return redirect()->back()->withErrors(['format' => 'Format Not Found.']);
	}
}