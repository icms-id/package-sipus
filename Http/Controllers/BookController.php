<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image;
use Uuid;
use Endroid\QrCode\QrCode;
use Package;
use Package\Nothing628\Sipus\Models\Pustaka;
use Package\Nothing628\Sipus\Models\PustakaList;
use Package\Nothing628\Sipus\Models\Penerbit;
use Package\Nothing628\Sipus\Models\Penulis;
use Package\Nothing628\Sipus\Models\Format;
use Package\Nothing628\Sipus\Models\Catalog;

class BookController extends Controller {

	public function index()
	{
		return Package::view('pustaka.list', [], 'Daftar Pustaka');
	}

	public function add()
	{
		return Package::view('pustaka.add', $this->paramView(), 'Tambah Pustaka');
	}

	public function save(Request $request)
	{
		$pustaka = new Pustaka;
		$pustaka->judul = $request->judul;
		$pustaka->deskripsi = $request->deskripsi;
		$pustaka->hrg = $request->harga;
		$pustaka->penerbit_id = $request->penerbit_id;
		$pustaka->penulis_id = $request->penulis_id;
		$pustaka->format_id = $request->format_id;
		$pustaka->catalog_id = $request->katalog_id;
		$pustaka->thn_terbit = $request->thn_terbit;
		$pustaka->cover = $this->processUpload($request);
		$pustaka->save();

		$pustaka->setKeyword($request->keyword);
		$pustaka->addPustaka($request->jumlah);

		return redirect()->to(Package::route('sipus.pustaka'));
	}

	public function ajax(Request $request)
	{
		$result = [];
		$pustakas = Pustaka::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $pustakas->count();
		$result['recordsFiltered'] = $pustakas->count();
		$result['data'] = [];

		$pustakas = $pustakas->splice($request->start ,$request->length);

		foreach ($pustakas as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Judul' => $value->judul,
				'Penulis' => $value->penulis->nama,
				'Penerbit' => $value->penerbit->nama,
				'Katalog' => $value->catalog->katalog,
				'Jumlah' => $value->listpustaka->count(),
			];
		}

		return response()->json($result);
	}

	public function ajaxlist($id, Request $request)
	{
		$result = [];
		$pustaka = Pustaka::find($id);

		$result['draw'] = $request->draw;
		$result['data'] = [];

		if ($pustaka) {
			$listpustaka = $pustaka->listpustaka;
			$result['recordsTotal'] = $listpustaka->count();
			$result['recordsFiltered'] = $listpustaka->count();

			$listpustaka = $listpustaka->splice($request->start, $request->length);

			foreach ($listpustaka as $value) {
				$result['data'][] = [
					'ID' => $value->id,
					'Kode' => $value->id.'-'.strtoupper(substr(md5($value->uuid),0,8)),
					'Judul' => $pustaka->judul,
					'Status' => $value->status == 1?'Tersedia':'Tidak Tersedia',
				];
			}
		} else {
			$result['recordsTotal'] = 0;
			$result['recordsFiltered'] = 0;
			$result['error'] = 'Pustaka not found';
		}

		return response()->json($result);
	}

	public function delete($id = null)
	{
		$pustaka = Pustaka::find($id);

		if ($pustaka) {
			$pustaka->delete();
		}

		return redirect()->to(Package::route('sipus.pustaka'));
	}

	public function deleteList($id = null)
	{
		$listpustaka = PustakaList::find($id);

		if ($listpustaka) {
			$package_id = $listpustaka->pustaka_id;
			$listpustaka->delete();

			return redirect()->to(Package::route('sipus.pustaka.detail', ['id' => $package_id]));
		}

		return redirect()->to(Package::route('sipus.pustaka'));
	}
	
	public function edit($id = null)
	{
		$pustaka = Pustaka::find($id);

		if ($pustaka) {
			return Package::view('pustaka.edit', $this->paramView(['pustaka' => $pustaka]), 'Edit Pustaka');
		}

		return redirect()->to(Package::route('sipus.pustaka'))->withErrors(['pustaka' => 'Pustaka not found.']);
	}

	public function update(Request $request)
	{
		$pustaka = Pustaka::find($request->id);

		if ($pustaka) {
			$pustaka->judul = $request->judul;
			$pustaka->deskripsi = $request->deskripsi;
			$pustaka->hrg = $request->harga;
			$pustaka->penerbit_id = $request->penerbit_id;
			$pustaka->penulis_id = $request->penulis_id;
			$pustaka->format_id = $request->format_id;
			$pustaka->catalog_id = $request->katalog_id;
			$pustaka->thn_terbit = $request->thn_terbit;
			$pustaka->cover = $this->processUpload($request, $pustaka->cover);
			$pustaka->save();

			$pustaka->setKeyword($request->keyword);

			return redirect()->to(Package::route('sipus.pustaka'));
		}

		return redirect()->back()->withErrors(['format' => 'Pustaka not Found.']);
	}

	public function detail($id = null)
	{
		$pustaka = Pustaka::find($id);

		if ($pustaka) {
			return Package::view('pustaka.detail', ['pustaka' => $pustaka], 'Detail Pustaka');
		}

		return redirect()->to(Package::route('sipus.pustaka'));
	}

	protected function paramView($arr = [])
	{
		$data = [];
		$data['format'] = Format::all();
		$data['penulis'] = Penulis::all();
		$data['penerbit'] = Penerbit::all();
		$data['katalog'] = Catalog::all();

		return array_merge($arr, $data);
	}

	protected function processUpload(Request $request, $default = null)
	{
		if ($request->hasFile('cover') && $request->has('crop_area')) {
			$file = $request->file('cover');
			$filename = Uuid::generate(4) . '.' . $file->getClientOriginalExtension();
			$path = storage_path('upload/' . $filename);
			$cover = Image::make($file);
			$croparea = json_decode($request->crop_area);

			$cover->crop((int) $croparea->width, (int) $croparea->height, (int) $croparea->x, (int) $croparea->y);
			$cover->save( $path ,70);

			return $filename;
		}

		return $default;
	}

	public function generateBarcode($id = null)
	{
		$listpustaka = PustakaList::find($id);

		if ($listpustaka) {
			$md = strtoupper(substr(md5($listpustaka->uuid),0,8));
			$qr = json_encode(['id' => $listpustaka->id, 'hash' => $md]);
			$qrCode = new QrCode();
			$qrCode
			    ->setText($qr)
			    ->setSize(350)
			    ->setPadding(10)
			    ->setErrorCorrection('high')
			    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
			    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
			    ->setLabel($listpustaka->id.'-'.$md)
			    ->setLabelFontSize(14)
			    ->setImageType(QrCode::IMAGE_TYPE_PNG);

			return response($qrCode->get())->header('Content-Type', $qrCode->getContentType());
		}

		abort(404);
	}

	public function addListPustaka($id = null, Request $request)
	{
		$pustaka = Pustaka::find($id);

		if ($pustaka) {
			$pustaka->addPustaka($request->jumlah);

			return redirect()->to(Package::route('sipus.pustaka.detail', ['id' => $id]));
		}

		return redirect()->to(Package::route('sipus.pustaka'));
	}
}