<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Package;

class AdminController extends Controller {
	public function index()
	{
		return Package::view('home', []);
	}
}