<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;
use Package\Nothing628\Sipus\Models\Penulis;

class PenulisController extends Controller {

	public function index()
	{
		return Package::view('penulis.list', [], 'Daftar Penulis');
	}

	public function add()
	{
		return Package::view('penulis.add', [], 'Tambah Penulis');
	}

	public function save(Request $request)
	{
		$penulis = new Penulis;
		$penulis->fill($request->all());
		$penulis->save();

		return redirect()->to(Package::route('sipus.penulis'));
	}

	public function ajax(Request $request)
	{
		$result = [];
		$penuliss = Penulis::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $penuliss->count();
		$result['recordsFiltered'] = $penuliss->count();
		$result['data'] = [];

		$penuliss = $penuliss->splice($request->start ,$request->length);

		foreach ($penuliss as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Nama' => $value->nama,
				'Telp' => $value->telepon,
				'Email' => $value->email,
			];
		}

		return response()->json($result);
	}

	public function delete($id = null)
	{
		$penulis = Penulis::find($id);

		if ($penulis) {
			$penulis->delete();
		}

		return redirect()->to(Package::route('sipus.penulis'));
	}
	
	public function edit($id = null)
	{
		$penulis = Penulis::find($id);

		if ($penulis) {
			return Package::view('penulis.edit', ['penulis' => $penulis], 'Edit Penulis');
		}

		return redirect()->to(Package::route('sipus.penulis'));
	}

	public function update(Request $request)
	{
		$penulis = Penulis::find($request->input('id'));

		if ($penulis) {
			$penulis->fill($request->all());
			$penulis->save();

			return redirect()->to(Package::route('sipus.penulis'));
		}

		return redirect()->back()->withErrors(['penulis' => 'Penulis Not Found.']);
	}
}