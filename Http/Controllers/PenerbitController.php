<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;
use Package\Nothing628\Sipus\Models\Penerbit;

class PenerbitController extends Controller {

	public function index()
	{
		return Package::view('penerbit.list', [], 'Daftar Penerbit');
	}

	public function add()
	{
		return Package::view('penerbit.add', [], 'Tambah Penerbit');
	}

	public function save(Request $request)
	{
		$penerbit = new Penerbit;
		$penerbit->fill($request->all());
		$penerbit->save();

		return redirect()->to(Package::route('sipus.penerbit'));
	}

	public function ajax(Request $request)
	{
		$result = [];
		$penerbits = Penerbit::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $penerbits->count();
		$result['recordsFiltered'] = $penerbits->count();
		$result['data'] = [];

		$penerbits = $penerbits->splice($request->start ,$request->length);

		foreach ($penerbits as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Nama' => $value->nama,
				'Telp' => $value->telepon,
				'Email' => $value->email,
			];
		}

		return response()->json($result);
	}

	public function delete($id = null)
	{
		$penerbit = Penerbit::find($id);

		if ($penerbit) {
			$penerbit->delete();
		}

		return redirect()->to(Package::route('sipus.penerbit'));
	}
	
	public function edit($id = null)
	{
		$penerbit = Penerbit::find($id);

		if ($penerbit) {
			return Package::view('penerbit.edit', ['penerbit' => $penerbit], 'Edit Penerbit');
		}

		return redirect()->to(Package::route('sipus.penerbit'));
	}

	public function update(Request $request)
	{
		$penerbit = Penerbit::find($request->input('id'));

		if ($penerbit) {
			$penerbit->fill($request->all());
			$penerbit->save();

			return redirect()->to(Package::route('sipus.penerbit'));
		}

		return redirect()->back()->withErrors(['penerbit' => 'Penerbit Not Found.']);
	}
}