<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Package;

class ReportController extends Controller {

	public function peminjaman()
	{
		return Package::view('report.peminjaman');
	}

	public function pengembalian()
	{
		return Package::view('report.pengembalian');
	}

	public function denda()
	{
		return Package::view('report.denda');
	}
}