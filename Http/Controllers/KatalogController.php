<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package\Nothing628\Sipus\Models\Catalog;
use Package;

class KatalogController extends Controller {

	public function index()
	{
		return Package::view('katalog.list', [], 'Daftar Katalog');
	}

	public function add()
	{
		return Package::view('katalog.add', [], 'Tambah Katalog');
	}

	public function save(Request $request)
	{
		$catalog = new Catalog;
		$catalog->fill($request->all());
		$catalog->save();

		return redirect()->to(Package::route('sipus.katalog'));
	}

	public function ajax(Request $request)
	{
		$result = [];
		$catalogs = Catalog::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $catalogs->count();
		$result['recordsFiltered'] = $catalogs->count();
		$result['data'] = [];

		$catalogs = $catalogs->splice($request->start ,$request->length);

		foreach ($catalogs as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Katalog' => $value->katalog,
				'Keterangan' => $value->keterangan,
			];
		}

		return response()->json($result);
	}

	public function delete($id = null)
	{
		$catalog = Catalog::find($id);

		if ($catalog) {
			$catalog->delete();
		}

		return redirect()->to(Package::route('sipus.katalog'));
	}

	public function edit($id = null)
	{
		$catalog = Catalog::find($id);

		if ($catalog) {
			return Package::view('katalog.edit', ['catalog' => $catalog], 'Edit Katalog');
		}

		return redirect()->to(Package::route('sipus.katalog'));
	}

	public function update(Request $request)
	{
		$catalog = Catalog::find($request->input('id'));

		if ($catalog) {
			$catalog->fill($request->all());
			$catalog->save();

			return redirect()->to(Package::route('sipus.katalog'));
		}

		return redirect()->back()->withErrors(['format' => 'Format Not Found.']);
	}
}