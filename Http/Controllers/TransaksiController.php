<?php 
namespace Package\Nothing628\Sipus\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Package;

class TransaksiController extends Controller {

	public function peminjaman()
	{
		return Package::view('transaksi.peminjaman', [], 'Transaksi Peminjaman');
	}

	public function pengembalian()
	{
		return Package::view('transaksi.pengembalian', [], 'Transaksi Pengembalian');
	}
}