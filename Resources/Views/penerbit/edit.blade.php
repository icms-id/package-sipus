@extends('base')

@section('title')
Penerbit <small>Edit Penerbit</small>
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post" action="{{ Package::route('sipus.penerbit.update') }}">
	{!! csrf_field() !!}
	<input type="hidden" name="id" value="{{ $penerbit->id }}">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Penerbit <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" required name="nama" value="{{ $penerbit->nama }}" maxlength="50">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Telp</label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input class="form-control" type="text" name="telepon" value="{{ $penerbit->telepon }}" maxlength="15">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="email" name="email" value="{{ $penerbit->email }}" maxlength="60">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea rows="2" class="form-control" name="alamat" maxlength="255">{{ $penerbit->alamat }}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea rows="2" class="form-control" name="keterangan" maxlength="255">{{ $penerbit->keterangan }}</textarea>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ route('sipus.penerbit') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>
@endsection