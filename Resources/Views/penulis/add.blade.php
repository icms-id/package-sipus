@extends('base')

@section('title')
Penulis <small>Tambah Penulis</small>
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post" action="{{ Package::route('sipus.penulis.save') }}">
	{!! csrf_field() !!}
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Penulis <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" required name="nama" maxlength="50">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Telp</label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input class="form-control" type="text" name="telepon" maxlength="15">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="email" name="email" maxlength="60">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea rows="2" class="form-control" name="alamat" maxlength="255"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea rows="2" class="form-control" name="keterangan" maxlength="255"></textarea>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ route('sipus.penulis') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>
@endsection