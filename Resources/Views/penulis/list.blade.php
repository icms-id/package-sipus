@extends('base')

@section('title')
Penulis <small>Daftar Penulis</small>
@endsection

@section('package.content')
<a href="{{ route('sipus.penulis.add') }}" class="btn btn-info"><i class="fa fa-pencil"></i> Add</a>
<table class="table table-bordered" id="data">
	<thead>
		<tr>
			<th>Nama</th>
			<th>Telp</th>
			<th>Email</th>
			<th>Action</th>
		</tr>
	</thead>
</table>
<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/dataTables.bootstrap.min.css') }}">

<script type="text/javascript" src="{{ Package::asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/responsive.bootstrap.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
        $('#data').DataTable({
			serverSide: true,
			ajax: {
				url: "{{ Package::route('sipus.penulis.ajax') }}",
				type: "POST"
			},
			columns: [
				{data: "Nama"},
				{data: "Telp"},
				{data: "Email"},
				{
					data: "ID",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = "<a href=\"{{ Package::route('sipus.penulis.edit') }}/"+oData.ID+"\">Edit</a>";
						content += " <a href=\"{{ Package::route('sipus.penulis.delete') }}/"+oData.ID+"\">Delete</a>";
						$(nTd).html(content);
					}
				}
			],
			responsive: true,
			ordering: false
		});
      });
</script>
@endsection