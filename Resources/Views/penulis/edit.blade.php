@extends('base')

@section('title')
Penulis <small>Edit Penulis</small>
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post" action="{{ Package::route('sipus.penulis.update') }}">
	{!! csrf_field() !!}
	<input type="hidden" name="id" value="{{ $penulis->id }}">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Penulis <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" required name="nama" maxlength="50" value="{{ $penulis->nama }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Telp</label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input class="form-control" type="text" name="telepon" maxlength="15" value="{{ $penulis->telepon }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="email" name="email" maxlength="60" value="{{ $penulis->email }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea rows="2" class="form-control" name="alamat" maxlength="255">{{ $penulis->alamat }}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea rows="2" class="form-control" name="keterangan" maxlength="255">{{ $penulis->keterangan }}</textarea>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ route('sipus.penulis') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>
@endsection