@extends('base')

@section('title')
Anggota <small>Edit Anggota</small>
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post" action="{{ Package::route('sipus.format.update') }}">
	{!! csrf_field() !!}
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ route('sipus.format') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>
@endsection