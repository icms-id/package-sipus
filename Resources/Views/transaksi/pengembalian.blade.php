@extends('base')

@section('title')
Pengembalian <small>Transaksi Pengembalian Buku</small>
@endsection

@section('package.content')
<form novalidate="" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Pustaka <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="input-group">
				<input class="form-control" type="text">
				<span class="input-group-btn">
					<button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Judul</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control" readonly="readonly">
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Peminjam</label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input type="text" class="form-control" readonly="readonly">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control" readonly="readonly">
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pinjam</label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input type="text" class="form-control" readonly="readonly">
		</div>
		<label class="control-label col-md-2 col-sm-2 col-xs-12">Tanggal Kembali</label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input type="text" class="form-control" readonly="readonly">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Denda</label>
		<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
			<input class="form-control has-feedback-left" id="inputSuccess2" type="text">
			<span class="form-control-feedback left">Rp.</span>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control" rows="2"></textarea>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Selesai</button>
		</div>
	</div>
</form>
@endsection