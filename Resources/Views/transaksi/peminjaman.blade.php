@extends('base')

@section('title')
Peminjaman <small>Transaksi Peminjaman Buku</small>
@endsection

@section('package.content')
<form novalidate="" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Peminjam <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="input-group">
				<input class="form-control" type="text">
				<span class="input-group-btn">
					<button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control" readonly="readonly">
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Pustaka <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="input-group">
				<input class="form-control" type="text">
				<span class="input-group-btn">
					<button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Judul</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control" readonly="readonly">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Kembali <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="input-prepend input-group">
				<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
				<input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="03/18/2013" />
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3">
			<button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</button>
		</div>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Judul</th>
				<th>Tanggal Kembali</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>12</td>
				<td>FAFAF</td>
				<td>12-2-2015</td>
				<td><a href="#"><i class="fa fa-times"></i> Delete</a></td>
			</tr>
		</tbody>
	</table>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Launch demo modal</button>
		</div>
	</div>
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped jambo_table">
        	<thead>
		<tr>
			<th>Judul</th>
			<th>Penulis</th>
			<th>Penerbit</th>
			<th>Jumlah</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>AAAAAA</td>
			<td>AAAAAA</td>
			<td>AAAAAA</td>
			<td>23</td>
			<td class="last"><a href="#">View</a></td>
		</tr>
	</tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script src="{{ Package::asset('js/moment.min.js') }}"></script>
<script src="{{ Package::asset('js/daterangepicker.js') }}"></script>
<script>
	$(document).ready(function() {
		var optionSet1 = {
          singleDatePicker: true,
          calender_style: "picker_3"
        };

		$('#reservation').daterangepicker(optionSet1, function(start, end, label) {});
		// $('#reservation').daterangepicker(null, function(start, end, label) {
		// 	console.log(start.toISOString(), end.toISOString(), label);
		// });
	});
</script>
@endsection