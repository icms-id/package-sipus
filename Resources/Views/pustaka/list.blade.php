@extends('base')

@section('title')
Pustaka <small>Daftar Pustaka</small>
@endsection

@section('package.content')
<a href="{{ route('sipus.pustaka.add') }}" class="btn btn-info"><i class="fa fa-pencil"></i> Add</a>
<table class="table table-bordered" id="data">
	<thead>
		<tr>
			<th>Judul</th>
			<th>Penulis</th>
			<th>Penerbit</th>
			<th>Katalog</th>
			<th>Jumlah</th>
			<th>Action</th>
		</tr>
	</thead>
</table>
<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/dataTables.bootstrap.min.css') }}">

<script type="text/javascript" src="{{ Package::asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/responsive.bootstrap.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#data').DataTable({
			serverSide: true,
			ajax: {
				url: "{{ Package::route('sipus.pustaka.ajax') }}",
				type: "POST"
			},
			columns: [
				{data: "Judul"},
				{data: "Penulis"},
				{data: "Penerbit"},
				{data: "Katalog"},
				{data: "Jumlah"},
				{
					data: "ID",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = "<a href=\"{{ Package::route('sipus.pustaka.detail') }}/"+oData.ID+"\">Detail</a> ";
						content += "<a href=\"{{ Package::route('sipus.pustaka.edit') }}/"+oData.ID+"\">Edit</a> ";
						content += "<a href=\"{{ Package::route('sipus.pustaka.delete') }}/"+oData.ID+"\">Delete</a>";
						$(nTd).html(content);
					}
				}
			],
			responsive: true,
			columnDefs: [
				{ responsivePriority: 1, targets: 0 },
				{ responsivePriority: 2, targets: 5 },
				{ responsivePriority: 3, targets: 4 },
				{ responsivePriority: 4, targets: 3 },
				{ responsivePriority: 5, targets: 2 },
			],
			ordering: false
		});
	});
</script>
@endsection