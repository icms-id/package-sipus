@extends('base')

@section('title')
Pustaka <small>Tambah Pustaka</small>
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post" enctype="multipart/form-data" action="{{ Package::route('sipus.pustaka.update') }}">
	{!! csrf_field() !!}
	<input type="hidden" name="id" value="{{ $pustaka->id }}">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Judul Pustaka <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" required name="judul" value="{{ $pustaka->judul }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea rows="2" class="form-control" name="deskripsi">{{ $pustaka->deskripsi }}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Satuan <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
			<input class="form-control has-feedback-left" required type="number" name="harga" value="{{ $pustaka->hrg }}">
			<span class="form-control-feedback left">Rp.</span>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Katalog <span class="required">*</span></label>
		<div class="col-md-5 col-sm-5 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" id="katalog-select" name="katalog_id">
				<option></option>
				@foreach ($katalog as $value)
				<option value="{{ $value->id }}" @if($value->id==$pustaka->catalog_id) selected @endif>{{ $value->id }} - {{ $value->katalog }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-1 col-sm-1 col-xs-12">
			<a class="btn btn-success" href="{{ Package::route('sipus.katalog.add') }}"><i class="fa fa-plus"></i></a>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Penulis</label>
		<div class="col-md-5 col-sm-5 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" id="penulis-select" name="penulis_id">
				<option></option>
				@foreach ($penulis as $value)
				<option value="{{ $value->id }}" @if($value->id==$pustaka->penulis_id) selected @endif>{{ $value->nama }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-1 col-sm-1 col-xs-12">
			<a class="btn btn-success" href="{{ Package::route('sipus.penulis.add') }}"><i class="fa fa-plus"></i></a>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Penerbit</label>
		<div class="col-md-5 col-sm-5 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" id="penerbit-select" name="penerbit_id">
				<option></option>
				@foreach ($penerbit as $value)
				<option value="{{ $value->id }}" @if($value->id==$pustaka->penerbit_id) selected @endif>{{ $value->nama }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-1 col-sm-1 col-xs-12">
			<a class="btn btn-success" href="{{ Package::route('sipus.penerbit.add') }}"><i class="fa fa-plus"></i></a>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun Terbit</label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<input class="form-control" type="text" maxlength="4" name="thn_terbit" value="{{ $pustaka->thn_terbit }}">
		</div>
		<label class="control-label col-md-2 col-sm-2 col-xs-12">Format Pustaka <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" id="format-select" name="format_id">
				<option></option>
				@foreach ($format as $value)
				<option value="{{ $value->id }}" @if($value->id==$pustaka->format_id) selected @endif>{{ $value->format }} - {{ $value->keterangan }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Alokasi Jumlah <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input class="form-control" type="number" required name="jumlah" value="{{ $pustaka->listpustaka->count() }}" readonly>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Keyword</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input id="tags_1" type="text" class="tags form-control" name="keyword" value="{{ $pustaka->getKeyword() }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Cover</label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<img src="{{ url('images/original/'.$pustaka->cover) }}" class="img-responsive">
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="img-container" style="width: 480px; height: 270px;">
				<img id="image" src="images/picture2.jpg" alt="Cover Image">
			</div>
			<input type="hidden" name="crop_area" id="crop-area">
			<label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
				<input class="sr-only" id="inputImage" name="cover" accept="image/*" type="file">
				<span data-original-title="Import image with Blob URLs" class="docs-tooltip" data-toggle="tooltip" title="">
					<span class="fa fa-upload"></span>
				</span>
			</label>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ Package::route('sipus.pustaka') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>

<link href="{{ Package::asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ Package::asset('css/cropper.min.css') }}" rel="stylesheet">
<script src="{{ Package::asset('js/select2.full.min.js') }}"></script>
<script src="{{ Package::asset('js/jquery.tagsinput.js') }}"></script>
<script src="{{ Package::asset('js/cropper.min.js') }}"></script>
<script>
	$(document).ready(function() {
		$("#katalog-select").select2({ placeholder: "Pilih Katalog", allowClear: true });
		$("#penulis-select").select2({ placeholder: "Pilih Penulis", allowClear: true });
		$("#penerbit-select").select2({ placeholder: "Pilih Penerbit", allowClear: true });
		$("#format-select").select2({ placeholder: "Pilih Format", allowClear: true });

		$('#tags_1').tagsInput({ width: 'auto' });

		var $image = $('#image');
		var $inputImage = $('#inputImage');
		var URL = window.URL || window.webkitURL;
		var blobURL;

		$image.cropper({
			aspectRatio: 2/3,
			guides:false,
			dragMode: 'none',
			modal: false,
			crop: function (data) {
				var newdata = {
					x: data.x,
					y: data.y,
					width: data.width,
					height: data.height,
					scaleX: data.scaleX,
					scaleY: data.scaleY
				};

				$('#crop-area').val(JSON.stringify(newdata));
			}
		});

		if (URL) {
		  $inputImage.change(function () {
			var files = this.files;
			var file;

			if (!$image.data('cropper')) {
			  return;
			}

			if (files && files.length) {
			  file = files[0];

			  if (/^image\/\w+$/.test(file.type)) {
				blobURL = URL.createObjectURL(file);
				$image.one('built.cropper', function () {

				  // Revoke when load complete
				  URL.revokeObjectURL(blobURL);
				}).cropper('reset').cropper('replace', blobURL);
			  } else {
				window.alert('Please choose an image file.');
			  }
			}
		  });
		} else {
		  $inputImage.prop('disabled', true).parent().addClass('disabled');
		}
	});
</script>
@endsection