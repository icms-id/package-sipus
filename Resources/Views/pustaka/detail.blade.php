<div class="">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Detail <small>Detail Pustaka</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Settings 1</a></li>
							</ul>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-md-2">
						<h2>Cover Image</h2>
						<img src="{{ url('images/original/'.$pustaka->cover) }}" class="img-responsive">
					</div>
					<div class="col-md-10">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Judul Pustaka</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<p class="form-control-static">{{ $pustaka->judul }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<p class="form-control-static">{{ $pustaka->deskripsi }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Satuan</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<p class="form-control-static">Rp. {{ number_format($pustaka->hrg) }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Katalog</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<p class="form-control-static">{{ $pustaka->catalog->katalog }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Penulis</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<p class="form-control-static"><a href="">{{ ($pustaka->penulis)?$pustaka->penulis->nama:null }}</a></p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Penerbit</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<p class="form-control-static"><a href="">{{ ($pustaka->penerbit)?$pustaka->penerbit->nama:null }}</a></p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun Terbit</label>
								<div class="col-md-2 col-sm-2 col-xs-12">
									<p class="form-control-static">{{ $pustaka->thn_terbit }}</p>
								</div>
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Format Pustaka</label>
								<div class="col-md-2 col-sm-2 col-xs-12">
									<p class="form-control-static">{{ $pustaka->format->format.' - '.$pustaka->format->keterangan }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Keyword</label>
								<div class="col-md-2 col-sm-2 col-xs-12">
									<p class="form-control-static">{{ $pustaka->getKeyword(', ') }}</p>
								</div>
								<label class="control-label col-md-2 col-sm-2 col-xs-12">Jumlah Pustaka</label>
								<div class="col-md-2 col-sm-2 col-xs-12">
									<p class="form-control-static">{{ $pustaka->listpustaka->count() }}</p>
								</div>
								<div class="col-md-1 col-sm-1 col-xs-12">
									<button type="button" id="add-pustaka" class="btn btn-success"><i class="fa fa-plus"></i></button>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
									<a href="{{ Package::route('sipus.pustaka') }}" class="btn btn-primary">Back</a>
									<a href="{{ Package::route('sipus.pustaka.edit', ['id' => $pustaka->id]) }}" class="btn btn-success">Edit</a>
									<a href="{{ Package::route('sipus.pustaka.delete', ['id' => $pustaka->id]) }}" class="btn btn-danger">Delete</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>List <small>List pustaka</small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Settings 1</a></li>
							</ul>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table table-bordered" id="data">
						<thead>
							<tr>
								<th># ID</th>
								<th>Judul</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/dataTables.bootstrap.min.css') }}">

<script type="text/javascript" src="{{ Package::asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/responsive.bootstrap.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#data').DataTable({
			dom: 'lBrtip',
			buttons: [
				{
					text: 'Print Barcode',
					action: function ( e, dt, node, config ) {
						alert( 'Activated!' );
						this.disable(); // disable button
					}
				}
			],
			serverSide: true,
			ajax: {
				url: "{{ Package::route('sipus.pustaka.list.ajax', ['id' => $pustaka->id]) }}",
				type: "POST"
			},
			columns: [
				{data: "Kode"},
				{data: "Judul"},
				{data: "Status"},
				{
					data: "ID",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = "<button class=\"btn btn-default btn-xs\" onclick=\"showBarcode("+oData.ID+")\">Show Barcode</button> ";
						content += "<a class=\"btn btn-danger btn-xs\" href=\"{{ Package::route('sipus.pustaka.list.delete') }}/"+oData.ID+"\">Delete</a>";
						$(nTd).html(content);
					}
				}
			],
			responsive: true,
			columnDefs: [
				{ responsivePriority: 1, targets: 0 },
				{ responsivePriority: 2, targets: -1 }
			],
			ordering: false
		});

		$('#modal-barcode').modal({show:false});
		$('#modal-pustaka').modal({show:false});

		$('#add-pustaka').click(function () {
			$('#modal-pustaka').modal('show');
		});

		$('#submit-form').click(function () {
			$('#form-add').submit();
		});
	});

	function urlBarcode(id) {
		var $base_url = '{{ Package::route('sipus.pustaka.list.barcode') }}/';
		var $url = $base_url + id;

		return $url;
	}

	function showBarcode(id) {
		var modal = $('#modal-barcode');
		var imgcontainer = $('#img-barcode');

		imgcontainer.attr('src', urlBarcode(id));console.log(modal);
		modal.modal('show');
	}
</script>

<!-- Modal Barcode -->
<div class="modal fade" id="modal-barcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Barcode</h4>
			</div>
			<div class="modal-body">
				<img src="" id="img-barcode" class="img-responsive">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Add Pustaka -->
<div class="modal fade" id="modal-pustaka" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Tambah Pustaka</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="{{ Package::route('sipus.pustaka.list.add', ['id' => $pustaka->id]) }}" id="form-add">
					{!! csrf_field() !!}
					<div class="form-group">
						<label class="control-label col-md-6 col-sm-6 col-xs-12">Jumlah Penambahan</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="jumlah" class="form-control">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-success" id="submit-form">Submit</button>
			</div>
		</div>
	</div>
</div>