@extends('base')

@section('title')
Katalog <small>Edit Katalog</small>
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post" action="{{ Package::route('sipus.katalog.update') }}">
	{!! csrf_field() !!}
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">ID Katalog <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<input class="form-control" type="text" maxlength="5" name="id" value="{{ $catalog->id }}" readonly>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Katalog <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" name="katalog" maxlength="60" value="{{ $catalog->katalog }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control" rows="2" name="keterangan" maxlength="255">{{ $catalog->keterangan }}</textarea>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ route('sipus.katalog') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>
@endsection