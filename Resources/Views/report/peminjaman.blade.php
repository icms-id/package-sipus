<div class="">
	<div class="page-title">
		<div class="title_left" style="height: 64px">
			<h3>Laporan Peminjaman</h3>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="row x_title">
					<div class="col-md-6"><h2>Peminjaman <small>Grafik Peminjaman</small></h2></div>
					<div class="col-md-6">
						<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
							<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
							<span>July 1, 2016 - July 30, 2016</span> <b class="caret"></b>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="mainb" style="height:400px;"></div>
					</div>
					<!-- <div class="col-md-3 col-sm-3 col-xs-3">
						<div class="x_title">
										<h2>Pustaka Popular</h2>
										<div class="clearfix"></div>
									</div>

									<div class="col-md-12 col-sm-12 col-xs-6">
										<div>
											<p>Facebook Campaign</p>
											<div class="">
												<div class="progress progress_sm" style="width: 76%;">
													<div aria-valuenow="79" style="width: 80%;" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>
												</div>
											</div>
										</div>
										<div>
											<p>Twitter Campaign</p>
											<div class="">
												<div class="progress progress_sm" style="width: 76%;">
													<div aria-valuenow="59" style="width: 60%;" class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-6">
										<div>
											<p>Conventional Media</p>
											<div class="">
												<div class="progress progress_sm" style="width: 76%;">
													<div aria-valuenow="39" style="width: 39%;" class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
												</div>
											</div>
										</div>
										<div>
											<p>Bill boards</p>
											<div class="">
												<div class="progress progress_sm" style="width: 76%;">
													<div aria-valuenow="29" style="width: 29%;" class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-6">
										<div>
											<p>Conventional Media</p>
											<div class="">
												<div class="progress progress_sm" style="width: 76%;">
													<div aria-valuenow="25" style="width: 25%;" class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
												</div>
											</div>
										</div>
									</div>

						<!- <h2 class="line_30">Pustaka Popular</h2>
						<table class="countries_list">
							<tbody>
								<tr>
									<td>United States</td>
									<td class="fs15 fw700 text-right">33%</td>
								</tr>
								<tr>
									<td>France</td>
									<td class="fs15 fw700 text-right">27%</td>
								</tr>
								<tr>
									<td>Germany</td>
									<td class="fs15 fw700 text-right">16%</td>
								</tr>
								<tr>
									<td>Spain</td>
									<td class="fs15 fw700 text-right">11%</td>
								</tr>
								<tr>
									<td>Britain</td>
									<td class="fs15 fw700 text-right">10%</td>
								</tr>
							</tbody>
						</table> -
					</div> -->
				</div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="row x_title">
					<div class="col-md-6"><h2>Peminjaman <small>Laporan Peminjaman</small></h2></div>
					<div class="col-md-6">
						<div id="reportrange2" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
							<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
							<span>July 1, 2016 - July 30, 2016</span> <b class="caret"></b>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Tgl</th>
								<th>Kunjungan</th>
								<th>Peminjaman</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1/1/2016</td>
								<td>100</td>
								<td>20</td>
							</tr>
							<tr>
								<td>1/2/2016</td>
								<td>100</td>
								<td>20</td>
							</tr>
						</tbody>
					</table>
					<button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{{ Package::asset('js/moment.min.js') }}"></script>
<script src="{{ Package::asset('js/daterangepicker.js') }}"></script>
<script src="{{ Package::asset('js/echarts.min.js') }}"></script>
<script src="{{ Package::asset('js/world.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {

				var cb = function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				};

				var optionSet1 = {
					startDate: moment().subtract(29, 'days'),
					endDate: moment(),
					minDate: '01/01/2012',
					maxDate: '12/31/2018',
					dateLimit: {
						days: 60
					},
					showDropdowns: false,
					showWeekNumbers: false,
					timePicker: false,
					timePickerIncrement: 1,
					timePicker12Hour: true,
					opens: 'left',
					format: 'MM/DD/YYYY',
				};
		$('#reportrange').daterangepicker(optionSet1, cb);
		$('#reportrange2').daterangepicker(optionSet1, cb);
		var theme = {
					color: [
							'#26B99A', '#34495E', '#BDC3C7', '#3498DB',
							'#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
					],

					title: {
							itemGap: 8,
							textStyle: {
									fontWeight: 'normal',
									color: '#408829'
							}
					},

					dataRange: {
							color: ['#1f610a', '#97b58d']
					},

					toolbox: {
							color: ['#408829', '#408829', '#408829', '#408829']
					},

					tooltip: {
							backgroundColor: 'rgba(0,0,0,0.5)',
							axisPointer: {
									type: 'line',
									lineStyle: {
											color: '#408829',
											type: 'dashed'
									},
									crossStyle: {
											color: '#408829'
									},
									shadowStyle: {
											color: 'rgba(200,200,200,0.3)'
									}
							}
					},

					dataZoom: {
							dataBackgroundColor: '#eee',
							fillerColor: 'rgba(64,136,41,0.2)',
							handleColor: '#408829'
					},
					grid: {
							borderWidth: 0
					},

					categoryAxis: {
							axisLine: {
									lineStyle: {
											color: '#408829'
									}
							},
							splitLine: {
									lineStyle: {
											color: ['#eee']
									}
							}
					},

					valueAxis: {
							axisLine: {
									lineStyle: {
											color: '#408829'
									}
							},
							splitArea: {
									show: true,
									areaStyle: {
											color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
									}
							},
							splitLine: {
									lineStyle: {
											color: ['#eee']
									}
							}
					},
					timeline: {
							lineStyle: {
									color: '#408829'
							},
							controlStyle: {
									normal: {color: '#408829'},
									emphasis: {color: '#408829'}
							}
					},

					k: {
							itemStyle: {
									normal: {
											color: '#68a54a',
											color0: '#a9cba2',
											lineStyle: {
													width: 1,
													color: '#408829',
													color0: '#86b379'
											}
									}
							}
					},
					map: {
							itemStyle: {
									normal: {
											areaStyle: {
													color: '#ddd'
											},
											label: {
													textStyle: {
															color: '#c12e34'
													}
											}
									},
									emphasis: {
											areaStyle: {
													color: '#99d2dd'
											},
											label: {
													textStyle: {
															color: '#c12e34'
													}
											}
									}
							}
					},
					force: {
							itemStyle: {
									normal: {
											linkStyle: {
													strokeColor: '#408829'
											}
									}
							}
					},
					chord: {
							padding: 4,
							itemStyle: {
									normal: {
											lineStyle: {
													width: 1,
													color: 'rgba(128, 128, 128, 0.5)'
											},
											chordStyle: {
													lineStyle: {
															width: 1,
															color: 'rgba(128, 128, 128, 0.5)'
													}
											}
									},
									emphasis: {
											lineStyle: {
													width: 1,
													color: 'rgba(128, 128, 128, 0.5)'
											},
											chordStyle: {
													lineStyle: {
															width: 1,
															color: 'rgba(128, 128, 128, 0.5)'
													}
											}
									}
							}
					},
					gauge: {
							startAngle: 225,
							endAngle: -45,
							axisLine: {
									show: true,
									lineStyle: {
											color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
											width: 8
									}
							},
							axisTick: {
									splitNumber: 10,
									length: 12,
									lineStyle: {
											color: 'auto'
									}
							},
							axisLabel: {
									textStyle: {
											color: 'auto'
									}
							},
							splitLine: {
									length: 18,
									lineStyle: {
											color: 'auto'
									}
							},
							pointer: {
									length: '90%',
									color: 'auto'
							},
							title: {
									textStyle: {
											color: '#333'
									}
							},
							detail: {
									textStyle: {
											color: 'auto'
									}
							}
					},
					textStyle: {
							fontFamily: 'Arial, Verdana, sans-serif'
					}
			};

		var echartBar = echarts.init(document.getElementById('mainb'), theme);

		echartBar.setOption({
				title: {
					text: 'Grafik Peminjaman',
					subtext: 'Peminjaman'
				},
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: ['kunjungan', 'peminjaman']
				},
				toolbox: {
					show: false
				},
				calculable: false,
				xAxis: [{
					type: 'category',
					data: ['1', '2', '3', '4', '5', '6', '7',
					 '8', '9', '10', '11', '12', '13', '14', '15',
					 '16', '17', '18', '19', '20', '21', '22', '23',
					 '24', '25', '26', '27', '28', '29', '30', '31']
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: 'kunjungan',
					type: 'line',
					itemStyle: {normal: {areaStyle: {type: 'default'}}},
					smooth: true,
					data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3, 0, 0, 0,
								0, 0, 0, 0, 0, 0, 0, 20, 40, 80, 100, 12, 0, 0, 0, 0],
					markPoint: {
						data: [{
							type: 'max',
							name: 'Maximum'
						}]
					},
					markLine: {
						data: [{
							type: 'average',
							name: 'Rata-Rata'
						}]
					}
				}, {
					name: 'peminjaman',
					type: 'bar',
					data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3, 12, 12, 12,
								90, 90, 0, 21, 22, 23, 56, 58, 88, 11, 99, 100, 120, 150, 111, 10],
					markPoint: {
						data: [{
							type: 'max',
							name: 'Maximum'
						}]
					},
					markLine: {
						data: [{
							type: 'average',
							name: 'Rata-Rata'
						}]
					}
				}]
			});

	});
</script>